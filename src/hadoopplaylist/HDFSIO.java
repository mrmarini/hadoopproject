/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hadoopplaylist;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;

/**
 * HDFS I/O helper
 *
 * @author mckomo
 */
public class HDFSIO {

    private FileSystem fileSystem;

    public HDFSIO(FileSystem fs) {
        fileSystem = fs;
    }

    public InputStreamReader openInputStream(Path inputPath) throws IOException {
        return new InputStreamReader(fileSystem.open(inputPath));
    }

    public OutputStreamWriter openOutputStream(Path outputPath) throws IOException {
        return new OutputStreamWriter(fileSystem.create(outputPath));
    }

    /**
     * Read object from given path
     *
     * @note After read object must be casteted to the original class
     * @param objectPath
     * @return Object
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Object readObject(Path objectPath) throws IOException, ClassNotFoundException {
        ObjectInputStream inputStream = new ObjectInputStream(fileSystem.open(objectPath));
        Object object = inputStream.readObject();
        inputStream.close();

        return object;
    }

    /**
     * Write object from given path
     *
     * @param objectPath
     * @param object
     * @throws IOException
     */
    public void writeObject(Path objectPath, Object object) throws IOException {
        ObjectOutputStream outStream = new ObjectOutputStream(fileSystem.create(objectPath));
        outStream.writeObject(object);
        outStream.close();
    }

    public void delete(Path file) throws IOException {
        fileSystem.delete(file, false);
    }

    /**
     *
     * @param tagetPath
     * @param dir
     * @param reducerCount
     */
    void mergeFiles(Path tagetPath, Path dir) throws IOException {

        FileUtil.copyMerge(
                fileSystem,
                dir,
                fileSystem,
                tagetPath,
                false,
                fileSystem.getConf(),
                ""
        );

//        // Create target file if doesn't exist so far
//        if (!fileSystem.exists(tagetPath)) {
//            fileSystem.createNewFile(tagetPath);
//        }
//        
//        OutputStreamWriter stream = openOutputStream( tagetPath );
//        stream.append("test");
//        stream.close();
//        
//        // Create list for files to merge
//        ArrayList<Path> files = new ArrayList<Path>();
//        // Get iterator of files in given dir
//        RemoteIterator<LocatedFileStatus> fileIterator = fileSystem.listFiles(dir, false);
//
//        while (fileIterator.hasNext()) {
//            files.add(fileIterator.next().getPath());
//        }
//        Path paths[] = new Path[files.size()];
//        files.toArray(paths);
//        fileSystem.concat(tagetPath, paths);
    }

}
