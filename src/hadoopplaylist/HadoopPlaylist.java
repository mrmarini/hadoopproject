package hadoopplaylist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.ToolRunner;

public class HadoopPlaylist {

    private static final String USERS_IN_COUNTRY_TMP_PATH = "/tmp_playlist/users_in_country.ser";
    private static final String SONGS_IN_COUNTRY_COUNT_TMP_PATH = "/tmp_playlist/songs_in_country.tsv";
    private static final String SONG_PLAY_COUNT_TMP_PATH = "/tmp_playlist/play_count";
    private static final String SONG_PLAY_SORT_TMP_PATH = "/tmp_playlist/play_sort";

    // Use this substring offset to obtain numeric value of user id from users list
    private static final Integer USER_SUBSTRING = 5;

    /**
     * Output information about script usage
     *
     * @example "hadoop jar HadoopPlaylist.jar /LFM/users /LFM/plays /LFM/pop 3
     * Italy|United Kingdom|China"
     * @return int
     */
    static int printUsage() {
        System.out.println("HadoopPlaylist <user_filename> <song_filename> <output> <playlist_size> <country_list>");
        ToolRunner.printGenericCommandUsage(System.out);
        return -1;
    }

    public static void main(String[] args) throws Exception {

        // Make sure there are exactly 4 parameters.
        if (args.length != 5) {
            System.out.printf("ERROR: Wrong number of parameters: %d instead of 5.\n", args.length);
            System.exit(printUsage());
        }

        Integer playlistSize = null;

        // Get size of playlist
        try {
            playlistSize = Integer.parseInt(args[3]);
        } catch (NumberFormatException except) {
            System.out.println("ERROR: Integer expected instead of " + args[3]);
            System.exit(printUsage());
        }

        // Create path for input of user list
        Path userInput = new Path(args[0]);
        // Create path for input of song list
        Path songInput = new Path(args[1]);
        // Create path for output of play counter job
        Path playCountPath = new Path(SONG_PLAY_COUNT_TMP_PATH);
        // Create path for merge result of play count
        Path playCountResutPath = new Path(SONG_PLAY_COUNT_TMP_PATH + "_result.tsv");
        // Create path for output of play sort job
        Path playSortPath = new Path(SONG_PLAY_SORT_TMP_PATH);
        // Create path for program output
        Path output = new Path(args[2]);

        // Get countries for playlists
        String countriesTemp[] = args[4].split("\\|");
        ArrayList<String> countries = new ArrayList<String>();
        for (String s : countriesTemp) {
            countries.add(PlaylistHelper.separateWithSpaces(s));
        }
        // Sort country list
        Collections.sort(countries);

        Configuration conf = new Configuration();

        // Create instance of HDFS I/O helper
        HDFSIO hdfsIo = new HDFSIO(FileSystem.get(conf));

        HashMap<String, ArrayList<Integer>> userInCountryMap
                = PlaylistHelper.getUsersFromCountries(countries, hdfsIo.openInputStream(userInput));

        // Store userInCountryMap in HDFS
        Path tmpUserMapPath = new Path(USERS_IN_COUNTRY_TMP_PATH);
        hdfsIo.writeObject(tmpUserMapPath, userInCountryMap);

        /*
         * Play count job
         */
        Job playCountJob = new Job(conf);

        FileInputFormat.setInputPaths(playCountJob, songInput);
        playCountJob.setInputFormatClass(TextInputFormat.class);
        playCountJob.setJarByClass(HadoopPlaylist.class);

        FileOutputFormat.setOutputPath(playCountJob, playCountPath);
        playCountJob.setMapOutputKeyClass(Text.class);
        playCountJob.setMapOutputValueClass(IntWritable.class);

        playCountJob.setOutputKeyClass(Text.class);
        playCountJob.setOutputValueClass(IntWritable.class);

        playCountJob.setMapperClass(PlayCountMapper.class);
        playCountJob.setCombinerClass(PlayCountReducer.class);
        playCountJob.setReducerClass(PlayCountReducer.class);

        playCountJob.waitForCompletion(true);

        // Merge result of prevoius job to one single file (to avoid total order sorting)
        hdfsIo.mergeFiles(playCountResutPath, playCountPath);

        /*
         * Play count sort job
         */
        Job playSortJob = new Job();

        playSortJob.setPartitionerClass(SongKeyPartitioner.class);
        playSortJob.setGroupingComparatorClass(SongKeyGroupingComparator.class);
        playSortJob.setSortComparatorClass(SongKeyComparator.class);
        
        playSortJob.setJarByClass(HadoopPlaylist.class);

        // Use output of previous job as input
        FileInputFormat.setInputPaths(playSortJob, playCountResutPath);
        playSortJob.setInputFormatClass(TextInputFormat.class);

        FileOutputFormat.setOutputPath(playSortJob, playSortPath);
        playSortJob.setMapOutputKeyClass(SongKey.class);
        playSortJob.setMapOutputValueClass(NullWritable.class);

        // Create seperate output file for each country
        for (String country : countries) {
            MultipleOutputs.addNamedOutput(playSortJob,
                    PlaylistHelper.removeSpaces(country),
                    TextOutputFormat.class,
                    Text.class,
                    NullWritable.class);
        }

        playSortJob.setOutputKeyClass(Text.class);
        playSortJob.setOutputValueClass(NullWritable.class);

        playSortJob.setMapperClass(PlaySortMapper.class);
        playSortJob.setCombinerClass(PlaySortReducer.class);
        playSortJob.setReducerClass(PlaySortReducer.class);

        playSortJob.waitForCompletion(true);

        // Write final ouput base of results of each country
        PlaylistHelper.writeResultToOutput(
                output,
                playlistSize,
                userInCountryMap,
                SONG_PLAY_SORT_TMP_PATH,
                hdfsIo
        );
    }

    /**
     * Getter for USER_SUBSTRING constant
     *
     * @return Intger
     */
    public static Integer getUserSubstring() {
        return USER_SUBSTRING;
    }

    /**
     * Getter for USERS_IN_COUNTRY_TMP_PATH constant
     *
     * @return String
     */
    public static String getUsersInCountryTmpPath() {
        return USERS_IN_COUNTRY_TMP_PATH;
    }

}
