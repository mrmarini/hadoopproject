package hadoopplaylist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Mapper for k-top song playlist job
 *
 * @author mckomo
 */
public class PlayCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private Text songKey = new Text();
    private final IntWritable oneValue = new IntWritable(1);

    private HashMap<String, ArrayList<Integer>> userInCountryMap;

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String s[] = value.toString().split("\t", -1);
        
        // Get numeric value of user's id
        Integer userId = Integer.parseInt(s[0].substring(HadoopPlaylist.getUserSubstring()));
        String songByArtist = s[5] + " by " + s[3];
        String countryKey = "";
        
        // Iterate each of given countries to check if user with obtained id belongs to that country
        for (Map.Entry<String, ArrayList<Integer>> e : userInCountryMap.entrySet()) {
            if (e.getValue().contains(userId)) {
                countryKey = e.getKey();
                break;
            }
        }

        // Skip mapping songs from users with unrelated countires
        if (countryKey.isEmpty()) {
            return;
        }

        songKey.set(countryKey + "\t" + songByArtist);
        context.write(songKey, oneValue);

    }

    @Override
    public void run(Context context) throws IOException, InterruptedException {
        super.run(context);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

        super.setup(context);
        
        // Get hash map of users sorted in tmp file
        HDFSIO hdfsIo = new HDFSIO(FileSystem.get(context.getConfiguration()));
        Path mapObjectPath = new Path(HadoopPlaylist.getUsersInCountryTmpPath());

        try {
            userInCountryMap = (HashMap<String, ArrayList<Integer>>) hdfsIo.readObject(mapObjectPath);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(HadoopPlaylist.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
