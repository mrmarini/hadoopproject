package hadoopplaylist;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Reducer for k-top playlist job
 *
 * @author mckomo
 */
public class PlayCountReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

    IntWritable playCount = new IntWritable(0);

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

        Integer sum = 0;

        for (IntWritable count : values) {
            sum += count.get();
        }

        playCount.set(sum);
        context.write(key, playCount);
    }

    @Override
    public void run(Context arg0) throws IOException, InterruptedException {
        super.run(arg0);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }
}
