package hadoopplaylist;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author mckomo
 */
public class PlaySortMapper extends Mapper<LongWritable, Text, SongKey, NullWritable> {

    private SongKey songKey = new SongKey();
    private final NullWritable nullValue = NullWritable.get();

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String s[] = value.toString().split("\t", -1);
        
        // Becouse country name will is used as name of output file, spaces have to be removed
        String country = PlaylistHelper.removeSpaces(s[0]);
        String songByArtist = s[1];
        Integer playCount = Integer.parseInt(s[2]);
        
        // Set properties of key
        songKey.setCountry(country);
        songKey.setSongByArtist(songByArtist);
        songKey.setPlayCount(playCount);
        
        context.write(songKey, nullValue);

    }

    @Override
    public void run(Context context) throws IOException, InterruptedException {
        super.run(context);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }
}
