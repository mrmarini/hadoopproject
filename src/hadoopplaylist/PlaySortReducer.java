package hadoopplaylist;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

/**
 *
 * @author mckomo
 */
public class PlaySortReducer extends Reducer<SongKey, NullWritable, Text, NullWritable> {

    private MultipleOutputs mos;
    private Text playlistKey = new Text();
    private final NullWritable nullValue = NullWritable.get();


    @Override
    protected void reduce(SongKey songKey, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
                
        // Use key with removed country
        playlistKey.set(songKey.toString());
        
        // Write entity to separate country file 
        mos.write(songKey.getCountry(), playlistKey, nullValue); 
    }

    @Override
    public void run(Context arg0) throws IOException, InterruptedException {
        super.run(arg0);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        mos = new MultipleOutputs(context);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
        mos.close();
    }
}
