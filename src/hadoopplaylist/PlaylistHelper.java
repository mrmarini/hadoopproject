package hadoopplaylist;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.hadoop.fs.Path;

/**
 *
 * @author mckomo
 */
public class PlaylistHelper {

    /**
     * Create hash map of given countries with country as a key and listed users
     * id as value
     *
     * @param countries
     * @param userStream
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    static public HashMap<String, ArrayList<Integer>> getUsersFromCountries(ArrayList<String> countries, InputStreamReader userStream) throws FileNotFoundException, IOException {

        HashMap<String, ArrayList<Integer>> userInCountryMap = new HashMap<String, ArrayList<Integer>>();

        BufferedReader br = new BufferedReader(userStream);
        String sCurrentLine;
        br.readLine();

        while ((sCurrentLine = br.readLine()) != null) {

            String s[] = sCurrentLine.split("\t", -1);

            Integer userIdInt = Integer.parseInt(s[0].substring(HadoopPlaylist.getUserSubstring()));
            String countryCandiate = s[3];

            // Skip unwanted countires
            if (!countries.contains(countryCandiate)) {
                continue;
            }

            ArrayList<Integer> uIds = (userInCountryMap.containsKey(countryCandiate) ? userInCountryMap.get(countryCandiate) : new ArrayList<Integer>());
            uIds.add(userIdInt);

            if (!userInCountryMap.containsKey(countryCandiate)) {
                userInCountryMap.put(countryCandiate, uIds);
            } else {
                userInCountryMap.remove(countryCandiate);
                userInCountryMap.put(countryCandiate, uIds);
            }
        }

        return userInCountryMap;
    }

    /**
     * Write pretty output base on results
     *
     * @param outputPath
     * @param playlistSize
     * @param resultPath
     * @param userInCountryMap
     * @param hdfsIo
     * @throws java.io.IOException
     */
    public static void writeResultToOutput(
            Path outputPath,
            int playlistSize,
            HashMap<String, ArrayList<Integer>> userInCountryMap,
            String resultPath,
            HDFSIO hdfsIo
    ) throws IOException {

        // Open write stream to output path
        OutputStreamWriter outputStream = hdfsIo.openOutputStream(outputPath);

        Path countryPlaylistPath;
        InputStreamReader countryPlaylistStream;

        StringBuilder outputBuilder = new StringBuilder();

        String currentLine;
        int i; // Used for playlist interation

        // Iterate each of given countries to write it's k-top playlist
        for (Map.Entry<String, ArrayList<Integer>> e : userInCountryMap.entrySet()) {

            // Get path for iterated country playlist
            countryPlaylistPath = new Path(resultPath + "/" + removeSpaces(e.getKey()) + "-m-00000");
            // Open read stream and buffer reader of a coutry playlist
            countryPlaylistStream = hdfsIo.openInputStream(countryPlaylistPath);
            BufferedReader playlistReader = new BufferedReader(countryPlaylistStream);

            // Add country header
            outputBuilder.append(
                    String.format(
                            "Popular songs in %s (based on %d users):\n",
                            e.getKey(),
                            e.getValue().size()
                    )
            );

            // Write first k-top songs of country playlist
            i = 0;
            while ((currentLine = playlistReader.readLine()) != null) {

                outputBuilder.append("\t" + currentLine + "\n");

                // Break reading after playlist has right size
                if (playlistSize == ++i) {
                    break;
                }
            }

            // Close input stream
            countryPlaylistStream.close();
        }

        // Write formated results to output stream
        outputStream.write(outputBuilder.toString());
        outputStream.close();
    }

    /**
     * Remove spaces from a string
     *
     * @example "Hello World" => "HelloWorld"
     * @param string
     * @return
     */
    public static String removeSpaces(String string) {
        return string.replaceAll(" ", "");
    }

    /**
     * Recover spaces from camel cased string
     *
     * @example "HelloWorld" => "Hello World"
     * @param string
     * @return
     */
    public static String separateWithSpaces(String string) {
        return string.replaceAll("(?!^)([A-Z])", " $1");
    }

}
