/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hadoopplaylist;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

/**
 *
 * @author mckomo
 */
public class SongKey implements WritableComparable<SongKey> {

    private String songByArtist;
    private String country;
    private Integer playCount;

    /**
     * Constructor without parameters
     */
    public SongKey() {
        songByArtist = "";
        country = "";
        playCount = 0;
    }

    /**
     * Constructor with parameters
     *
     * @param country
     * @param songByArtist
     * @param playCount
     */
    public SongKey(String country, String songByArtist, Integer playCount) {
        this.country = country;
        this.songByArtist = songByArtist;
        this.playCount = playCount;
    }

    @Override
    public String toString() {
        return String.format("Song: %s, %d plays", songByArtist, playCount);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        String keyPart[] = WritableUtils.readString(in).split("\t", -1);
        country = keyPart[0];
        songByArtist = keyPart[1];
        playCount = Integer.parseInt(keyPart[2]);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        WritableUtils.writeString(out, country + "\t" + songByArtist + "\t" + playCount);
    }

    @Override
    public int compareTo(SongKey o) {
        int result = country.compareTo(o.country);
        if (0 == result) {
            result = playCount.compareTo(o.playCount);
        }
        return result;
    }

    public String getIdentifier() {
        return country + "\t" + songByArtist;
    }

    public String getSongByArtist() {
        return songByArtist;
    }

    public void setSongByArtist(String songByArtist) {
        this.songByArtist = songByArtist;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPlayCount() {
        return playCount;
    }

    public void setPlayCount(Integer playCount) {
        this.playCount = playCount;
    }

}
