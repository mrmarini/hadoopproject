package hadoopplaylist;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 *
 * @author mckomo
 */
public class SongKeyComparator extends WritableComparator {

    /**
     * Constructor
     */
    protected SongKeyComparator() {
        super(SongKey.class, true);
    }

    @Override
    public int compare(WritableComparable w1, WritableComparable w2) {

        SongKey k1 = (SongKey) w1;
        SongKey k2 = (SongKey) w2;

        int result = k1.getCountry().compareTo(k2.getCountry());

        if (0 == result) {
            result = -1 * k1.getPlayCount().compareTo(k2.getPlayCount());
        }

        return result;
    }
}
