package hadoopplaylist;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * Grouping comparator for SongKey
 *
 * @author mckomo
 */
public class SongKeyGroupingComparator extends WritableComparator {

    /**
     * Constructor
     */
    protected SongKeyGroupingComparator() {
        super(SongKey.class, true);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public int compare(WritableComparable w1, WritableComparable w2) {
        SongKey k1 = (SongKey) w1;
        SongKey k2 = (SongKey) w2;

        return k1.getIdentifier().compareTo(k2.getIdentifier());
    }
}
