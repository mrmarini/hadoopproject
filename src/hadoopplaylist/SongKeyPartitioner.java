/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hadoopplaylist;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * Partitioner for SongKey
 *
 * @author mckomo
 */
public class SongKeyPartitioner extends Partitioner<SongKey, NullWritable> {

    @Override
    public int getPartition(SongKey key, NullWritable val, int numPartitions) {
        return 0; // Ensure to use only one reducer (one output file as result) 
    }

}
